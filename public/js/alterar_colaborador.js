$(document).ready(function() {
	
	$('#txtNome').change(function(){
		var valorRecebido = $('#txtNome').val();
		$('#txtVerificaConteudo').val(valorRecebido);					
	});
	
	$('#txtEmail').change(function(){
		var valorRecebido = $('#txtEmail').val();
		$('#txtVerificaConteudo').val(valorRecebido);					
	});
	
	$('#txtLogin').change(function(){
		var valorRecebido = $('#txtLogin').val();
		$('#txtVerificaConteudo').val(valorRecebido);					
	});
	
	$('#txtSenha').change(function(){
		var valorRecebido = $('#txtSenha').val();
		$('#txtVerificaConteudo').val(valorRecebido);				
	});
	
	$('#txtConfirmaSenha').change(function(){
		var valorRecebido = $('#txtConfirmaSenha').val();
		$('#txtVerificaConteudo').val(valorRecebido);				
	});	
			
	$('#slcSetor').change(function(){
		var valorRecebido = $('#slcSetor option:selected').text();
		$('#txtVerificaConteudo').val(valorRecebido);					
	});	
	
	$('#slcTipo').change(function(){
		var valorRecebido = $('#slcTipo option:selected').text();
		$('#txtVerificaConteudo').val(valorRecebido);				
	});	
	
});