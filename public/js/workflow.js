$(document).ready(function() {
	if($('#msgVazia').is(':empty')){				
		$('#msgSucesso').hide();
	}else{
		$('#myModal').modal('show');
	} 
});

var contEtapas = 0;
var posicao = 1;

function adicionaEtapa(){
	var inputDescricao = document.getElementById("txtDescricaoEtapa");
	var tabela = document.getElementById("table_etapas");
	tabela.innerHTML += '<tr>'
		+'<td><div class="col-md-2"><input class ="input-no-border" type="text" name="workflow.etapas['+contEtapas+'].posicao" id="idEtapa" value="' + posicao +'"></div></td>'
        +'<td><input class ="input-no-border" type=text name="workflow.etapas['+contEtapas+'].descricao" id="workflow.etapas['+contEtapas+'].descricao" value="'+inputDescricao.value+'"></td>'
        +'<td><input type="button" class="btn btn-danger btn-xs" value="Excluir" onclick="javascript:removeLinha(this)"></td></tr>'
	contEtapas++;
	posicao++;
	inputDescricao.value = "";
	inputDescricao.focus();
}
