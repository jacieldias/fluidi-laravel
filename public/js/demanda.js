/* Códigos Jquery referentes ao comportamento da view do cadastro das tarefas.
---------------------------------------------------------------------------------------------------------------------*/

/* Código para definir o comportamento da mensagem quando a view for chamada. 
---------------------------------------------------------------------------------------------------------------------*/
$(document).ready(function() {

    if ($('#msgVazia').is(':empty')) {
        $('#msgSucesso').hide();
    } else {
        $('#myModal').modal('show');
    }

    /* Código para as mascáras dos inputs. 
    ---------------------------------------------------------------------------------------------------------------------*/
    $('#txtDataAbertura').mask('99/99/9999');
    $('#txtDataEncerramento').mask('99/99/9999');
    /*  
    ---------------------------------------------------------------------------------------------------------------------*/
    var comparaLogado = $('#txtColabLogado').val();
    var comparaCriador = $('#txtColabCriador').val();
    var comparaExecutor = $('#txtColabExecutor').val();
    var status = $('#txtSituacaoStatus').val();

    if (comparaLogado == comparaCriador && comparaLogado == comparaExecutor && status == 'FINALIZADA') {

        $('#btnEncerrar').attr('disabled', 'disabled');
        $('#btnIniciar').attr('disabled', 'disabled');
        $('#btnAlterar').removeAttr('disabled');
    } else
    if (comparaLogado == comparaCriador && comparaLogado == comparaExecutor && status == 'EM ANDAMENTO') {

        $('#btnIniciar').removeAttr('disabled');
        $('#btnIniciar').attr('disabled', 'disabled');
    } else
    if (comparaLogado == comparaCriador && comparaLogado == comparaExecutor && status == 'PENDENTE') {

        $('#btnEncerrar').removeAttr('disabled');
        $('#btnIniciar').removeAttr('disabled');
    } else
    if (comparaLogado == comparaCriador && comparaLogado != comparaExecutor && status == 'FINALIZADA') {

        $('#btnEncerrar').attr('disabled', 'disabled');
        $('#btnIniciar').attr('disabled', 'disabled');
        $('#btnAlterar').removeAttr('disabled');
    } else
    if (comparaLogado == comparaCriador && comparaLogado != comparaExecutor && status != 'FINALIZADA') {

        $('#btnEncerrar').attr('disabled', 'disabled');
        $('#btnIniciar').attr('disabled', 'disabled');
        $('#btnAlterar').removeAttr('disabled');
    } else
    if (comparaLogado != comparaCriador && comparaLogado == comparaExecutor && status == 'FINALIZADA') {

        $('#btnEncerrar').attr('disabled', 'disabled');
        $('#btnIniciar').attr('disabled', 'disabled');
        $('#btnAlterar').attr('disabled', 'disabled');
    } else
    if (comparaLogado != comparaCriador && comparaLogado == comparaExecutor && status != 'FINALIZADA') {

        $('#btnEncerrar').removeAttr('disabled');
        $('#btnAlterar').attr('disabled', 'disabled');
    } else
    if (comparaLogado != comparaCriador && comparaLogado != comparaExecutor && status == 'FINALIZADA') {

        $('#btnEncerrar').attr('disabled', 'disabled');
        $('#btnIniciar').attr('disabled', 'disabled');
        $('#btnAlterar').attr('disabled', 'disabled');
    } else
    if (comparaLogado != comparaCriador && comparaLogado != comparaExecutor && status != 'FINALIZADA') {

        $('#btnEncerrar').attr('disabled', 'disabled');
        $('#btnAlterar').attr('disabled', 'disabled');
    }

    /*  
    ---------------------------------------------------------------------------------------------------------------------*/

    $(function() {

        var data = new Date();

        dia = data.getDate(),
            mes = data.getMonth() + 1;
        ano = data.getFullYear();

        var dataFormatada = dia + "/" + mes + "/" + ano;
        $('#txtDataAbertura').val(dataFormatada);
    })

});