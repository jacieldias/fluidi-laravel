$(function() {
	
	 table = $('#tbl_setor').DataTable(
			{
			 "lengthMenu" : [5, 15, 25],
			 "paging" : true,
			 "ordering" : true,
			 "info" : true,				 
			 "aoColumnDefs" : [{"orderable" : false, "targets" : 2}],	
			 "language" : {"url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"}
		    });

		if($('#msgVazia').is(':empty')){ 
			
			    $('#myModal').modal('hide');
		    }else{
			    $('#myModal').modal('show');
		} 		

	$('[data-toggle="popover"]').popover();
	 
});