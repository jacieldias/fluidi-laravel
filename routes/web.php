<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/login/login');
});

Route::group(['as' => 'index.', 'prefix' => 'index'], function(){
    Route::get('index', ['as' => 'index', 'uses' => 'IndexController@index']);
});

Route::group(['as' => 'login.', 'prefix' => 'login'], function(){
    Route::post('login', ['as' => 'login', 'uses' => 'LoginController@index']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@create']);
});

Route::group(['as' => 'setor.', 'prefix' => 'setores'], function(){
    
    Route::get('index',['as' => 'index', 'uses' => 'SetorController@index']);
    Route::get('create',['as' => 'create', 'uses' => 'SetorController@create']);
    Route::post('save',['as' => 'store', 'uses' => 'SetorController@store']);
    Route::get('{id}/edit',['as' => 'edit', 'uses' => 'SetorController@edit']);
    Route::post('{id}/update',['as' => 'update', 'uses' => 'SetorController@update']);
    Route::get('{id}/remove',['as' => 'destroy', 'uses' => 'SetorController@destroy']);
    
});

Route::group(['as' => 'colaborador.', 'prefix' => 'colaboradores'], function(){
    
    Route::get('index',['as' => 'index', 'uses' => 'ColaboradorController@index']);
    Route::get('create',['as' => 'create', 'uses' => 'ColaboradorController@create']);
    Route::get('search',['as' => 'show', 'uses' => 'ColaboradorController@show']);
    Route::post('save',['as' => 'store', 'uses' => 'ColaboradorController@store']);
    Route::get('{id}/edit',['as' => 'edit', 'uses' => 'ColaboradorController@edit']);
    Route::post('{id}/update',['as' => 'update', 'uses' => 'ColaboradorController@update']);
    Route::get('{id}/remove',['as' => 'destroy', 'uses' => 'ColaboradorController@destroy']);
    
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
