@extends('welcome')

@section('titulo', 'Alterar Colaborador')

@section('content')
	<link type="text/css" rel="stylesheet" media="screen" href="{{ asset('/css/css/estilos_gerais.css') }}"/>
	<link type="text/css" rel="stylesheet" media="screen" href="{{ asset('/css/css/alterar_colaborador.css') }}"/>
	<script type="text/javascript" charset="utf8" src="{{ asset('/js/alterar_colaborador.js') }}"></script>
	<title>Alterar Colaborador</title>

@if(session()->has('flash_message'))  
<!-- Modal Mensagem de Sucesso -->
	<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<h4 class="modal-title">Atenção!</h4>
					</div>
					<div class="modal-body">
						<div id="msgSucesso">
		  		            <p id="msgVazia">{{ session()->get('flash_message')['msg'] }}</p> 
		  	            </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Ok</button>
					</div>
				</div>
			</div>
		</div>
@endif        	
	<h3>Alterar Colaborador</h3>
		<hr>
		<br>
		<form id="frm_altera_colaborador" role="form" method="post" action="/colaboradores/{{$colaborador->id}}/update">						
			{{ csrf_field() }}
            <input type="hidden" name="_method" value="post">	
                <div class="row">				
					<div class="col-md-3">
						<div class="form-group {{ $errors->has('nome') ? 'has-error' : '' }}">					
							<label for="txtNome">Nome:</label> 
							<input type="text" 	class="form-control input-sm" id="txtNome" name="nome" value="{{ $colaborador->nome }}">
							<input type="hidden" class="form-control" id="txtId" name="id" value="{{ $colaborador->id }}">
							<input type="hidden" class="form-control" id="txtVerificaConteudo" name="verificaConteudoCampo">
							<span class="help-block">{{ $errors->first('nome') }}</span>
						</div>
					</div>	
					<div class="col-md-3">
						<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">					
							<label for="txtEmail">Email:</label> 
							<input type="text" class="form-control input-sm" id="txtEmail" name="email" value="{{ $colaborador->email }}">
							<span class="help-block">{{ $errors->first('email') }}</span>
						</div>
					</div>	
					<div class="col-md-2">
						<div class="form-group {{ $errors->has('login') ? 'has-error' : '' }}">					
							<label for="txtLogin">Login:</label> 
							<input type="text" 	class="form-control input-sm" id="txtLogin" name="login" value="{{ $colaborador->login }}" readonly="readonly">
							<span class="help-block">{{ $errors->first('login') }}</span>
						</div>
					</div>						
					<div class="col-md-2">
						<div class="form-group {{ $errors->has('senha') ? 'has-error' : '' }}">					
							<label for="txtSenha">Senha:</label> 
							<input type="password" 	class="form-control input-sm" id="txtSenha" name="senha" value="{{ $colaborador->senha }}" readonly="readonly">
							<span class="help-block">{{ $errors->first('senha') }}</span>
						</div>
					</div>	
					<div class="col-md-2">
						<div class="form-group {{ $errors->has('confirmaSenha') ? 'has-error' : '' }}">					
							<label for="txtConfirmaSenha">Confirmação Senha:</label> 
							<input type="password" 	class="form-control input-sm" id="txtConfirmaSenha" name="confirmaSenha" value="{{ $colaborador->confirmaSenha }}" readonly="readonly">
							<span class="help-block">{{ $errors->first('confirmaSenha') }}</span>
						</div>
					</div>
				</div>
					<div class="row">	
						<div class="col-md-3">
							<div class="form-group {{ $errors->has('setor_id') ? 'has-error' : '' }}">						
								<label for="slcSetor">Setor:</label> 
								<select class="form-control input-sm" id="slcSetor" name="setor_id">
									<option value="{{ $colaborador->setor->id }}">{{ $colaborador->setor->descricao }}</option>
									@foreach ($setores as $setor)									
										<option value="{{ $setor->id }}">{{ $setor->descricao }}</option>									
									@endforeach						
								</select>
								<span class="help-block">{{ $errors->first('setor_id') }}</span>
							</div>
						</div>						
						<div class="col-md-3">
							<div class="form-group {{ $errors->has('tipoColaborador') ? 'has-error' : '' }}">						
								<label for="slcTipo">Tipo:</label> 
								<select class="form-control input-sm" id="slcTipo" name="tipoColaborador">
									<option value="{{ $colaborador->tipoColaborador }}">{{ $colaborador->tipoColaborador }}</option>
									<option value="Interno">Interno</option>
									<option value="Externo">Externo</option>
								</select>
								<span class="help-block">{{ $errors->first('tipoColaborador') }}</span>
							</div>
						</div>															
			    </div>	
			<button id="btnSalvar" type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
			<a href="{{ route('colaborador.show') }}" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</a>
		</form>	
	<br>
@endsection    


