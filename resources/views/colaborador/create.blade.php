@extends('welcome')

@section('titulo','Novo colaborador')

@section('content')
	<link type="text/css" rel="stylesheet" media="screen" href="{{ asset('/css/css/estilos_gerais.css') }}"/>	
	<script type="text/javascript" charset="utf8" src="{{ asset('/js/colaborador.js') }}"></script>
	
@if(session()->has('flash_message'))    
<!-- Modal Mensagem de Sucesso -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<h4 class="modal-title">Atenção!</h4>
				</div>
				<div class="modal-body">
					<div id="msgSucesso">
						<p id="msgVazia">{{ session()->get('flash_message')['msg'] }}</p> 						
						<p id="opcoesModal">O que deseja fazer:</p>						
					</div>
				</div>
				<div class="modal-footer">					
					<a href="{{ route('colaborador.show') }}" class="btn btn-info">
						<span class="glyphicon glyphicon-search"></span> Consultar
					</a>
					<button type="button" class="btn btn-success" data-dismiss="modal">
						<span class="glyphicon glyphicon-plus"></span> Cadastrar
					</button>
					<a href="{{ route('index.index') }}" class="btn btn-primary">
						<span class="glyphicon glyphicon-remove"></span> Sair
					</a>					
				</div>
			</div>
		</div>		
	</div>
@endif	
		<h3>Novo Colaborador</h3>
		<hr>
		<br>			
		<form id="frmColaborador" role="form" method="post" action="{{ route('colaborador.store') }}">
         {{ csrf_field() }}							
				<div class="row">					
					<div class="col-md-3">
					    <div class="form-group {{ $errors->has('nome') ? 'has-error' : '' }}">												
							<label for="txtNome">Nome:</label> 
							<input type="text" class="form-control input-sm" id="txtNome" name="nome" value="{{ old('nome') }}">							
							 	<span class="help-block">{{ $errors->first('nome') }}</span>
						</div>
					</div>	
					<div class="col-md-3">
					    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">												
							<label for="txtEmail">Email:</label> 
							<input type="text" class="form-control input-sm" id="txtEmail" name="email" value="{{ old('email') }}">							
								<span class="help-block">{{ $errors->first('email') }}</span>							
						</div>
					</div>
					<div class="col-md-2">
					    <div class="form-group {{ $errors->has('login') ? 'has-error' : '' }}">												
							<label for="txtLogin">Login:</label> 
							<input type="text" class="form-control input-sm" id="txtLogin" name="login" value="{{ old('login') }}">						    
								<span class="help-block">{{ $errors->first('login') }}</span>											
						</div>
					</div>	
					<div class="col-md-2">
					    <div class="form-group {{ $errors->has('senha') ? 'has-error' : '' }}">												
							<label for="txtSenha">Senha:</label> 
							<input type="password" class="form-control input-sm" id="txtSenha" name="senha" value="{{ old('senha') }}">							
								<span class="help-block">{{ $errors->first('senha') }}</span>							
						</div>
					</div>
					<div class="col-md-2">
					    <div class="form-group {{ $errors->has('confirmaSenha') ? 'has-error' : '' }}">												
							<label for="txtConfirmaSenha">Confirme a senha:</label> 
							<input type="password" class="form-control input-sm" id="txtConfirmaSenha" name="confirmaSenha" value="{{ old('confirmaSenha') }}">							
								<span class="help-block">{{ $errors->first('confirmaSenha') }}</span>							
						</div>
					</div>		
				</div>
					<div class="row">					
						<div class="col-md-3">	
						    <div class="form-group {{ $errors->has('setor_id') ? 'has-error' : '' }}">													
								<label for="slcSetor">Setor:</label> 
								<select class="form-control input-sm" id="slcSetor" name="setor_id">
									<option></option>
									@foreach ($setores as $setor)									
										<option value="{{ $setor->id }}">{{ $setor->descricao }}</option>									
									@endforeach
								</select>								
									<span class="help-block">{{ $errors->first('setor_id') }}</span>								
							</div>
					    </div>					
					    <div class="col-md-3">
					        <div class="form-group {{ $errors->has('tipoColaborador') ? 'has-error' : '' }}">						
								<label for="slcTipo">Tipo:</label> 
								<select class="form-control input-sm" id="slcTipo" name="tipoColaborador">
										<option></option>
										<option value="Interno">Interno</option>
										<option value="Externo">Externo</option>
								</select>								
									<span class="help-block">{{ $errors->first('tipoColaborador') }}</span>								
							</div>
						</div>
					</div>													
			    		<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
		</form>		
@endsection  