@extends('welcome')

@section('titulo','Novo Setor')

@section('content')
<link type="text/css" rel="stylesheet" media="screen" href="{{ asset('/css/css/estilos_gerais.css') }}"/>
<link type="text/css" rel="stylesheet" media="screen" href="{{ asset('/css/css/setor.css') }}"/>

<link type="text/css" rel="stylesheet" media="screen" href="{{ asset('/datatables/media/css/dataTables.bootstrap.css') }}">
<script type="text/javascript" charset="utf8" src="{{ asset('/datatables/media/js/jquery.js') }}"></script>
<script type="text/javascript" charset="utf8" src="{{ asset('/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" charset="utf8" src="{{ asset('/datatables/media/js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" charset="utf8" src="{{ asset('/css/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" charset="utf8" src="{{ asset('/js/setor.js') }}"></script>

@if(session()->has('flash_message'))
<!-- Modal Mensagem de Sucesso -->
	<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<h4 class="modal-title">Atenção!</h4>
					</div>
					<div class="modal-body">
						<div id="msgSucesso">
		  		            <p id="msgVazia">{{ session()->get('flash_message')['msg'] }}</p> 
		  	            </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Ok</button>
					</div>
				</div>

			</div>
	</div>	
@endif	
		<h3>Novo Setor</h3>		
		<hr>
		<form id="formSetor" role="form" method="post" action="{{ route('setor.store') }}">
	        {{ csrf_field() }}
			<div class="form-group {{ $errors->has('descricao') ? 'has-error' : '' }}">				
				<label for="txtDescricao">Descrição:</label> 
				<input type="text" class="form-control input-sm" id="txtDescricao" name="descricao" value="{{ old('descricao') }}">				
					<span class="help-block">{{ $errors->first('descricao') }}</span>				
			</div>	
			<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
		</form>
		<br>
	<!-- Tabela Refente ao Cadastro dos Dados do Setor Plugin usado para formatação da tabela: DataTable 1.10.7-->		
		<table id="tbl_setor"
			class="table table-striped table-bordered table-condensed table-hover">
			<thead>
				<tr>
					<th width="10%">Id</th>
					<th>Descrição</th>
					<th width="10%">Ações</th>
				</tr>
			</thead>
			<tbody>					
				@foreach ($setores as $setor)			
					<tr>
						<td id="tdId">{{ $setor->id }}</td>
						<td id="tdDescricao">{{ $setor->descricao }}</td>
						<td id="tdAcoes">
							<a href="/setores/{{$setor->id}}/edit" class="btn btn-primary btn-xs"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Alterar Setor">
							<span class="glyphicon glyphicon-edit"></span></a>							
							<!--
							<a href="#" class="btn btn-danger btn-xs btnexcluir"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Excluir Setor">
							<span class="glyphicon glyphicon-trash"></span></a>
							-->						
						</td>
					</tr>				
				@endforeach
			</tbody>
		</table>
@endsection    

