@extends('welcome')

@section('titulo','Excluir Setor')
	
@section('content')
<link type="text/css" rel="stylesheet" media="screen" href="{{ asset('/css/bootstrap/css/bootstrap.min.css') }}"/>
<script type="text/javascript" charset="utf8" src="{{ asset('/js/jquery-2.1.4.js') }}"></script>	
<script type="text/javascript" charset="utf8" src="{{ asset('/css/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">

	$(document).ready(function() {
	
		$('#myModalExcluir').modal('show');
	
	});	

</script>


	     <!-- Modal Mensagem Remove -->
      <div id="myModalExcluir" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal" >&times;</button> -->
						<h4 class="modal-title">Atenção!</h4>
					</div>
					<div class="modal-body">
							<div id="msgSucesso">
		  		                 <p id="msgVaziaExcluir">Deseja realmente excluir o setor: ${setor.descricao}?</p> 
		  	                </div>
					</div>
					<div class="modal-footer">
							<input type="hidden" class="form-control input-sm" id="txtDescricao" name="setor.descricao" value="${setor.descricao}"> 
					        <input type="hidden" class="form-control" id="txtId" name="setor.id" value="${setor.id}">
					        <input type="hidden" class="form-control" id="txtVerificaConteudo">
															
			               	<a href="${linkTo[SetorController].remove(setor.id, setor.descricao)}" class="btn btn-danger"><span class="glyphicon glyphicon glyphicon-ok"></span> Excluir</a>
			                <a href="${linkTo[SetorController].formulario}" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</a>
			        </div>
				</div>
			</div>
		</div>			
