@extends('welcome')

@section('titulo','Alterar Setor')

@section('content')
<head>	
	<link type="text/css" rel="stylesheet" media="screen" href="{{ asset('/css/css/estilos_gerais.css') }}">
	<link type="text/css" rel="stylesheet" media="screen" href="{{ asset('/css/css/alterar_setor.css') }}">
	<script type="text/javascript" charset="utf8" src="{{ asset('/js/alterar_setor.js') }}"></script>
	<title>Alterar Setor</title>
</head>
@if(session()->has('flash_message'))
<!-- Modal Mensagem de Sucesso -->
	<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<h4 class="modal-title">Atenção!</h4>
					</div>
					<div class="modal-body">
							<div id="msgSucesso">
		  		                 <p id="msgVazia">{{ session()->get('flash_message')['msg'] }}</p> 
		  	                </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Ok</button>
					</div>
				</div>
			</div>
		</div>	
@endif				
		<h3>Alterar Setor</h3>
		<hr>
		<form id="formAlterar" role="form" method="post" action="/setores/{{$setor->id}}/update">
		 {{ csrf_field() }}
		 <input type="hidden" name="_method" value="post">
			<div class="form-group {{ $errors->has('descricao') ? 'has-error' : '' }}">
				<label for="txtDescricao">Descrição:</label> 
					<input type="text" class="form-control input-sm" id="txtDescricao" name="descricao" value="{{ $setor->descricao }}"> 
					<span class="help-block">{{ $errors->first('descricao') }}</span>
					<input type="hidden" class="form-control" id="txtId" name="id" value="{{ $setor->id }}">
					<input type="hidden" class="form-control" id="txtVerificaConteudo" name="verificaConteudoCampo">
			</div>
			<button id="btnSalvar" type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
			<a href="{{ route('setor.create') }}" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</a>
		</form>
	<br>
@endsection
