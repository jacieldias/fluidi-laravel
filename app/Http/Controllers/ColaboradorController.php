<?php

namespace Fluidi\Http\Controllers;

use Fluidi\Colaborador;
use Fluidi\Setor;
use Illuminate\Http\Request;
use Fluidi\Http\Requests;
use Fluidi\Http\Requests\ColaboradorRequest;

class ColaboradorController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        $setores = Setor::all(); 
        return view('colaborador.create', compact('setores'));          
    }

    public function store(ColaboradorRequest $request)
    {
        $colaborador = new Colaborador();        
        $colaborador->nome = $request->get('nome');
        $colaborador->login = $request->get('login');
        $colaborador->senha = $request->get('senha');
        $colaborador->confirmaSenha = $request->get('confirmaSenha');
        $colaborador->email = $request->get('email');
        $colaborador->tipoColaborador = $request->get('tipoColaborador');
        $colaborador->setor_id = $request->get('setor_id');
        $colaborador->save();
        
        $request->session()->flash('flash_message',['msg'=>"Colaborador " . $colaborador->nome . " adicionado com Sucesso!"]);
        
        return redirect()->route('colaborador.create');
    }

    public function show()
    {
        $colaboradores = Colaborador::all();          
        return view('colaborador.show', compact('colaboradores'));        
    }

    public function edit($id)    {
        $setores = Setor::all();
        $colaborador = Colaborador::find($id);
        return view('colaborador.edit', compact('colaborador', 'setores'));  
    }

    public function update(ColaboradorRequest $request, $id)
    {
        $colaborador = Colaborador::find($id);             
        $colaborador->nome = $request->get('nome');
        $colaborador->login = $request->get('login');
        $colaborador->senha = $request->get('senha');
        $colaborador->confirmaSenha = $request->get('confirmaSenha');
        $colaborador->email = $request->get('email');
        $colaborador->tipoColaborador = $request->get('tipoColaborador');
        $colaborador->setor_id = $request->get('setor_id');
        $colaborador->save();

        $verificaConteudoCampo = $request->get('verificaConteudoCampo');

        if($verificaConteudoCampo == ''){
             return redirect()->route('colaborador.show');
        }
        else
        {   
            $request->session()->flash('flash_message',['msg'=>"Colaborador " . $colaborador->nome . " alterado com Sucesso!"]);    

            return redirect()->route('colaborador.show');

        }
    }

    public function destroy($id)
    {
        //
    }
}
