<?php

namespace Fluidi\Http\Controllers;

use Fluidi\Setor;
use Illuminate\Http\Request;
use Fluidi\Http\Requests;
use Fluidi\Http\Requests\SetorRequest;

class SetorController extends Controller
{   
    
    public function index()
    {
         return view('setor.edit');
    }
    
    public function create()
    {
        $setores = Setor::all();              
        return view('setor.create', compact('setores'));    
    }
    
    public function store(SetorRequest $request)
    {   
        $setor = new Setor();        
        $setor->descricao = $request->get('descricao');
        $setor->save();
        
        $request->session()->flash('flash_message',['msg'=>"Setor " . $setor->descricao . " adicionado com Sucesso!"]);
        
        return redirect()->route('setor.create'); 
               
    }
    
    public function show(Setor $setor)
    {
        
    }
    
    public function edit($id)
    {
        $setor = Setor::find($id);
        return view('setor.edit', compact('setor'));              
    }
    
    public function update(SetorRequest $request, $id)
    {
        $setor = Setor::find($id);
        $setor->descricao = $request->get('descricao');
        $setor->save();

        $verificaConteudoCampo = $request->get('verificaConteudoCampo');

        if($verificaConteudoCampo == ''){
             return redirect()->route('setor.create');
        }
        else
        {   
            $request->session()->flash('flash_message',['msg'=>"Setor " . $setor->descricao . " alterado com Sucesso!"]);    

            return redirect()->route('setor.create');
        }        
         
    }
    
    public function destroy(Setor $setor)
    {
        
    }
}
