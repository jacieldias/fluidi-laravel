<?php

namespace Fluidi\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    
    public function index()
    {
        return view('index.index');
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
