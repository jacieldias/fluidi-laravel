<?php

namespace Fluidi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SetorRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'descricao' => 'required|max:30'
        ];
    }

    public function messages()
    {

        return [
            'required' => 'O campo :attribute não pode ser vazio!'
    ];

    }
}
