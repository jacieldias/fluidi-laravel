<?php

namespace Fluidi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ColaboradorRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required',
            'login' => 'required',
            'senha' => 'required',
            'confirmaSenha' => 'required|same:senha',
            'email' => 'Nullable|email',
            'tipoColaborador' => 'required',
            'setor_id' => 'required',
        ];
    }

    public function messages()
    {

        return [
            'required' => 'O campo :attribute não pode ser vazio!',
            'setor_id.required' => 'O campo setor não pode ser vazio!',
            'tipoColaborador.required' => 'O campo tipo não pode ser vazio!'
    ];

    }
}
