<?php

namespace Fluidi;

use Illuminate\Database\Eloquent\Model;

class Colaborador extends Model
{
    protected $fillable = [

            'nome',
            'login',
            'senha',
            'confirmaSenha',
            'email',
            'tipoColaborador',
            'setor_id', 
    ];

    public function setor()
    {
        return $this->belongsTo('Fluidi\Setor');
    }
}
